import difflib
import logging
import os
import socket


def install_summain(ctx):
    runcmd_prepend_to_path = globals()["runcmd_prepend_to_path"]
    srcdir = globals()["srcdir"]
    bindir = os.path.join(srcdir, "target", "debug")
    runcmd_prepend_to_path(ctx, dirname=bindir)


def create_directory(ctx, dirname=None):
    os.mkdir(dirname)


def create_file(ctx, filename=None):
    open(filename, "w").close()


def create_symlink(ctx, linkname=None, target=None):
    os.symlink(target, linkname)


def create_socket(ctx, filename=None):
    fd = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    fd.bind(filename)


def create_fifo(ctx, filename=None):
    os.mkfifo(filename)


def set_mode(ctx, filename=None, mode=None):
    os.chmod(filename, int(mode, 8))


def set_atime(ctx, filename=None, timestamp=None):
    st = os.lstat(filename)
    os.utime(filename, (int(timestamp), int(st.st_mtime)), follow_symlinks=False)


def set_mtime(ctx, filename=None, timestamp=None):
    st = os.lstat(filename)
    os.utime(filename, (int(st.st_atime), int(timestamp)), follow_symlinks=False)


def output_matches_file(ctx, filename=None):
    runcmd_get_stdout = globals()["runcmd_get_stdout"]
    get_file = globals()["get_file"]
    assert_eq = globals()["assert_eq"]

    actual = runcmd_get_stdout(ctx)
    expected = get_file(filename).decode("UTF-8")

    diff = "".join(
        line.rstrip("\n") + "\n"
        for line in difflib.unified_diff(
            expected.splitlines(),
            actual.splitlines(),
            fromfile="expected",
            tofile="actual",
        )
    )
    logging.debug("output_matches:")
    logging.debug(f"  actual:\n{actual}")
    logging.debug(f"  expect:\n{expected}")
    logging.debug(f"  diff:\n{diff}")
    if actual != expected:
        print("ERROR: diff:")
        print(diff)
    assert_eq(actual, expected)
