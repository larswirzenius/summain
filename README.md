---
title: Summain
...

# Summain

Produce a manifest of specified files. The manifest lists the files,
and for each file its important metadata, and if it's a regular file,
the checksum of the contents. The order in the output is
deterministic: if the program is run twice for the same file, and
files haven't changed, the output is identical.

This is meant for testing that a backup has been restored correctly.


# Legalese

Copyright 2020  Lars Wirzenius

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
